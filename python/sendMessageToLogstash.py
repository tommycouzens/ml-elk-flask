import socket
import json
import sys

HOST = '127.0.0.1'
PORT = 5000

def sendMessagesToLogstash(HOST, PORT, message):
  try:    
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  except socket.error as msg:
    sys.stderr.write("[ERROR] %s\n" % msg[1])
    sys.exit(1)

  try:
    sock.connect((HOST, PORT))
  except socket.error as msg:
    sys.stderr.write("[ERROR] %s\n" % msg[1])
    sys.exit(2)

  for msg in messages:
    sock.send(msg.encode('utf-8'))

  sock.close()


messages = ["hello", "how are you?", "good to see you", "yes you too", "it's been so long"]
sendMessagesToLogstash(HOST, PORT, messages)

sys.exit(0)